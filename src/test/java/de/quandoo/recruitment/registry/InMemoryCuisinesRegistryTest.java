package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;

public class InMemoryCuisinesRegistryTest {
	private InMemoryCuisinesRegistry cuisinesRegistry;
	private AtomicLong customerIdGenerator = new AtomicLong();

	@Before
	public void setup() {
		cuisinesRegistry = new InMemoryCuisinesRegistry();
	}

	@Test
	public void shouldWork1() {
		Customer c1, c2, c3;
		cuisinesRegistry.register(c1 = customer(), cuisine("french"));
		cuisinesRegistry.register(c2 = customer(), cuisine("german"));
		cuisinesRegistry.register(c3 = customer(), cuisine("italian"));

		assertCuisineCustomers(cuisine("french"), c1);
		assertCuisineCustomers(cuisine("german"), c2);
		assertCuisineCustomers(cuisine("italian"), c3);
	}

	@Test(expected = NullPointerException.class)
	public void shouldWork2() {
		cuisinesRegistry.cuisineCustomers(null);
	}

	@Test(expected = NullPointerException.class)
	public void shouldWork3() {
		cuisinesRegistry.customerCuisines(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTopCuisinesIllegalArgument() {
		cuisinesRegistry.topCuisines(0);
	}
	
	@Test
	public void testTopCuisines() {
		assertTopCuisines(1);
		
		cuisinesRegistry.register(customer(), cuisine("italian"));

		assertTopCuisines(1, "italian");
		assertTopCuisines(2, "italian");

		cuisinesRegistry.register(customer(), cuisine("french"));
		cuisinesRegistry.register(customer(), cuisine("french"));

		assertTopCuisines(1, "french");
		assertTopCuisines(2, "french", "italian");
		assertTopCuisines(3, "french", "italian");

		cuisinesRegistry.register(customer(), cuisine("thai"));
		cuisinesRegistry.register(customer(), cuisine("thai"));
		cuisinesRegistry.register(customer(), cuisine("thai"));
		assertTopCuisines(3, "thai", "french", "italian");

		cuisinesRegistry.register(customer(), cuisine("french"));
		cuisinesRegistry.register(customer(), cuisine("french"));
		assertTopCuisines(3, "french", "thai", "italian");
	}
	
	/**
	 * Tests that registering the same customer with the same cuisine does not affect the cuisine's Top-N rating.
	 */
	@Test
	public void testTopCuisinesSameCustomer() {
		Customer c0 = customer();
		Customer c1 = customer();
		
		cuisinesRegistry.register(c0, cuisine("italian"));
		assertTopCuisines(1, "italian");
		
		cuisinesRegistry.register(customer(), cuisine("french"));
		cuisinesRegistry.register(c1, cuisine("french"));
		
		assertTopCuisines(2, "french", "italian");
		
		// already registered customer must have no effect
		cuisinesRegistry.register(c0, cuisine("italian"));
		cuisinesRegistry.register(c0, cuisine("italian"));
		
		assertTopCuisines(2, "french", "italian");
	}

	@Test
	public void testCustomerCuisines() {
		Customer c0 = customer();	// one customer

		assertCustomerCuisines(c0);	// has no cuisines yet

		cuisinesRegistry.register(c0, cuisine("french"));
		assertCustomerCuisines(c0, "french");

		cuisinesRegistry.register(c0, cuisine("french"));	// try to register french cuisine twice
		assertCustomerCuisines(c0, "french");	// is registered only once

		cuisinesRegistry.register(c0, cuisine("italian"));
		assertCustomerCuisines(c0, "french", "italian");

		cuisinesRegistry.register(c0, cuisine("Italian"));		// case sensitive!
		assertCustomerCuisines(c0, "french", "italian", "Italian");

	}

	@Test
	public void testCuisineCustomers() {
		Cuisine french = cuisine("french");
		Customer c0 = customer();

		// has no customers yet
		assertCuisineCustomers(french);

		cuisinesRegistry.register(c0, french);
		assertCuisineCustomers(french, c0);

		cuisinesRegistry.register(c0, french);	// try to register french cuisine twice
		assertCuisineCustomers(french, c0);	// is registered only once

		Customer c1 = customer();
		cuisinesRegistry.register(c1, french);
		assertCuisineCustomers(french, c0, c1);	// two customers for french cuisine

		Customer c2 = customer();
		Cuisine italian = cuisine("italian");
		cuisinesRegistry.register(c2, italian);
		assertCuisineCustomers(french, c0, c1);	// two customers for french cuisine
		assertCuisineCustomers(italian, c2);	// one customer for italian cuisine
	}
	
	void assertTopCuisines(int n, String... cuisines) {
		List<Cuisine> actual = cuisinesRegistry.topCuisines(n);
		if(cuisines.length == 0)
			assertThat(actual, is(empty()));
		else
			assertThat(actual, equalTo(Arrays.stream(cuisines).map(Cuisine::new).collect(Collectors.toList())));
	}

	/**
	 * Retrieves all cuisines for the specified customer and compares the result with the expected cuisines. The order is not relevant.
	 * 
	 * @param customer customer
	 * @param cuisines expected cuisines for the customer; if not specified, no cuisines are expected to be registered
	 */
	void assertCustomerCuisines(Customer customer, String... cuisines) {
		List<Cuisine> actual = cuisinesRegistry.customerCuisines(customer);
		if(cuisines.length == 0)
			assertThat(actual, is(empty()));
		else
			assertThat(actual, containsInAnyOrder(Arrays.stream(cuisines).map(Cuisine::new).toArray(Cuisine[]::new)));
	}

	/**
	 * Retrieves all customers for the specified cuisine and compares the result with the expected customers. The order is not relevant.
	 * 
	 * @param cuisine cuisine
	 * @param customers expected customers for the cuisine; if not specified, no customers are expected to be registered
	 */
	void assertCuisineCustomers(Cuisine cuisine, Customer... customers) {
		List<Customer> actual = cuisinesRegistry.cuisineCustomers(cuisine);
		if(customers.length == 0)
			assertThat(actual, is(empty()));
		else
			assertThat(actual, containsInAnyOrder(customers));
	}

	/**
	 * Creates a new Customer with a new unique ID for testing purposes.
	 * 
	 * @return never {@code null}
	 */
	Customer customer() {
		return new Customer(customerIdGenerator.getAndIncrement() + "");
	}

	/**
	 * Creates a new Cuisine with the specified description.
	 * 
	 * @param desc cuisine description
	 * @return never {@code null}
	 */
	Cuisine cuisine(String desc) {
		return new Cuisine(desc);
	}
}