package de.quandoo.recruitment.registry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import de.quandoo.recruitment.registry.model.Cuisine;

public class CuisineTest {
	@Test
	public void shouldWork1() {
		Cuisine c1 = new Cuisine("french");
		Cuisine c1b = new Cuisine("french");
		Cuisine c2 = new Cuisine("german");
		
		assertEquals(c1, c1);
		assertEquals(c1, c1b);
		assertNotEquals(c1, c2);
	}
}