package de.quandoo.recruitment.registry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import de.quandoo.recruitment.registry.model.Customer;

public class CustomerTest {
	@Test
	public void shouldWork1() {
		Customer c1 = new Customer("1");
		Customer c1b = new Customer("1");
		Customer c2 = new Customer("2");
		
		assertEquals(c1, c1);
		assertEquals(c1, c1b);
		assertNotEquals(c1, c2);
	}
}