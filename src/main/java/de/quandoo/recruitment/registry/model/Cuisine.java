package de.quandoo.recruitment.registry.model;

import lombok.Value;

@Value
public class Cuisine {
	private final String name;

	public Cuisine(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}