package de.quandoo.recruitment.registry;

import java.util.*;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimaps;
import com.google.common.util.concurrent.AtomicLongMap;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {
	private HashMultimap<Customer, Cuisine> registry = HashMultimap.create();	// Guava Multimap is able to store multiple values for the same key
	private AtomicLongMap<Cuisine> cuisineUsage = AtomicLongMap.create();	// for top cuisines search

	/**
	 * Registers the cuisine with the customer. Multiple calls have 
	 * @see de.quandoo.recruitment.registry.api.CuisinesRegistry#register(de.quandoo.recruitment.registry.model.Customer, de.quandoo.recruitment.registry.model.Cuisine)
	 * @throws NullPointerException if provided {@code Customer} or {@code Cuisine} is null
	 */
	@Override
	public void register(final Customer customer, final Cuisine cuisine) {
		Preconditions.checkNotNull(customer, "Provided Customer is null!");
		Preconditions.checkNotNull(cuisine, "Provided Cuisine is null!");
		
		if(registry.put(customer, cuisine))
			cuisineUsage.incrementAndGet(cuisine);
	}

	@Override
	public List<Customer> cuisineCustomers(final Cuisine cuisine) {
		Preconditions.checkNotNull(cuisine, "Provided Cuisine is null!");
		
		HashMultimap<Cuisine, Customer> inverse = Multimaps.invertFrom(registry, HashMultimap.<Cuisine, Customer>create());
		return new ArrayList<Customer>(inverse.get(cuisine));
	}

	@Override
	public List<Cuisine> customerCuisines(final Customer customer) {
		Preconditions.checkNotNull(customer, "Provided Customer is null!");
		
		return new ArrayList<Cuisine>(registry.get(customer));
	}

	@Override
	public List<Cuisine> topCuisines(final int n) {
		Preconditions.checkArgument(n > 0, "N must be greater than 0 for top N query!");
		
		// @formatter:off
		return cuisineUsage.asMap().entrySet().stream()
				.sorted(Map.Entry.<Cuisine, Long>comparingByValue().reversed())
				.limit(n)
				.map(Map.Entry::getKey)
				.collect(Collectors.toList()); 
		// @formatter:on
	}
}